import machine
import time
G = machine.Pin (23,machine.Pin.OUT)
B = machine.Pin (2,machine.Pin.OUT)
R = machine.Pin (21,machine.Pin.OUT)
button = machine.Pin (19,machine.Pin.IN, machine.Pin.PULL_DOWN)
n = 0
while True:
    before = button.value()
    time.sleep(0.005)
    if button.value() == 1 and before == 0:
        n = n + 1
    else:
        n = n
    print(n)
    if n == 4:
       n = 1
    else:
        n = n
    if n == 1:
        G.value (1)
        R.value (0)
        B.value (0)
    if n == 2:
        G.value (0)
        R.value (1)
        B.value (0)
    if n == 3:
        G.value (0)
        R.value (0)
        B.value (1)
    