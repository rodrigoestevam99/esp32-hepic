# Gia rápido de Git

Site com guia rápido: https://rogerdudler.github.io/git-guide/index.pt_BR.html

## Comandos básicos de Unix

Quando utilizamos o `git bash` estamos recriando um terminal unix para navegar pelos arquivos e utilizar comandos `git`. Por isso na primeira seção vamos aos comandos unix basicos.

Uma referencia para mais comandos: https://www.devmedia.com.br/comandos-importantes-linux/23893

**PWD**

Retrona o caminho do diretório atual
```
>> pwd
/home/rodrigo/cosmic/esp32-hepic
```

**LS**

Lista as pastas e arquivos do diretório atual
```
>> pwd
/home/rodrigo/cosmic
>> ls
Cosmic-Binder  esp32-hepic  raioscosmicos.gitlab.io
```

**CD**

Muda de diretório
```
>> pwd
/home/rodrigo

# Avançando um diretório
>> cd cosmic
>> pwd
/home/rodrigo/cosmic

# Voltando um diretorio
>> cd ..
/home/rodrigo

# Mudando de diretório com caminho absoluto
>> cd /home/rodrigo/cosmic/esp32-hepic
>> pwd
/home/rodrigo/cosmic/esp32-hepic

```

**MKDIR**

Cria um diretório
```
>> pwd
/home/rodrigo/cosmic
>> ls
Cosmic-Binder  esp32-hepic  raioscosmicos.gitlab.io
>> mkdir test
>> ls
Cosmic-Binder  esp32-hepic  raioscosmicos.gitlab.io  test    
```
## Comandos básicos de Git

**Git Clone**

Método mais usual de se iniciar um repositório de Git. Como você normalmente vai começar utilizando um repositório de GitHub ou GitLab, é necessário primeiro ir até o website do repositório e copiar o endereço de clone

```
# Exemplo com esse repositório
>> git clone https://gitlab.com/rodrigoestevam99/esp32-hepic.git
```


**Git Status**

Retorna o status do seus arquivos em comparação com a origin
```
>> git status
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")


```


**Git Add**

Adiciona arquivos ao próximo commit

**Git Commit**


**Git Push**

**Git Pull**



